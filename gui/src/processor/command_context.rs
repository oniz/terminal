use std::io::{Read, Write};
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::{Duration};
use portable_pty::{Child, CommandBuilder, MasterPty, NativePtySystem, PtySize, PtySystem};
use tokio::task;
use wezterm_term::{Terminal, TerminalSize};
use crate::config::config::DyvytConfig;

pub struct CommandContext {
    size: TerminalSize,
    pub terminal: Arc<Mutex<Terminal>>,
    pub master: Box<dyn MasterPty + Send>,
    pub child: Box<dyn Child + Send + Sync>,
}

impl CommandContext {
    pub fn new(size: TerminalSize, command: String) -> anyhow::Result<Self> {
        let pty = NativePtySystem::default();
        let pair = pty.openpty(PtySize {
            rows: size.rows as u16,
            cols: size.cols as u16,
            pixel_height: 0,
            pixel_width: 0,
        })?;
        let child = pair.slave.spawn_command(spawn_builder())?;
        // Release any handles owned by the slave: we don't need it now
        // that we've spawned the child.
        drop(pair.slave);
        
        let mut terminal_writer = pair.master.take_writer()?;
        if cfg!(target_os = "macos") {
            // macOS quirk: the child and reader must be started and
            // allowed a brief grace period to run before we allow
            // the writer to drop. Otherwise, the data we send to
            // the kernel to trigger EOF is interleaved with the
            // data read by the reader! WTF!?
            // This appears to be a race condition for very short
            // lived processes on macOS.
            // I'd love to find a more deterministic solution to
            // this than sleeping.
            thread::sleep(Duration::from_millis(20));
        }
        
        if !command.trim().is_empty() {
            terminal_writer.write_all(command.as_bytes()).unwrap_or_default();
        }
        
        let terminal = Arc::new(Mutex::new(Terminal::new(
            size,
            Arc::new(DyvytConfig::default()),
            "Dyvyt",
            "0.1",
            terminal_writer,
        )));
        
        let mut terminal_reader = pair.master.try_clone_reader()?;
        let term = terminal.clone();
        
        task::spawn_blocking(move || {
            let mut buf = [0u8; 4120];
            loop {
                // read from terminal
                match terminal_reader.read(&mut buf) {
                    Ok(n) => {
                        if n > 0 {
                            let mut guard = term.lock().unwrap();
                            (*guard).advance_bytes(&buf[..n]);
                        }
                    }
                    Err(e) => {
                        eprintln!("Read from terminal error. {e}");
                        break;
                    }
                }
            }
        });
        
        Ok(Self {
            size,
            terminal: terminal.clone(),
            master: pair.master,
            child,
        })
    }
}

impl Drop for CommandContext {
    fn drop(&mut self) {
        self.child.kill().unwrap_or(());
    }
}

fn spawn_builder() -> CommandBuilder {
    #[cfg(unix)]
    {
        let mut cmd = CommandBuilder::new("bash");
        // necessary for interactive mode (meaning the shell will not exit immediately after running the first command)
        cmd.arg("-i");
        cmd
    }
    #[cfg(windows)]
    {
        // let command = "pwsh -NoProfile -NonInteractive -NoLogo";
        let mut cmd = CommandBuilder::new("pwsh");
        // cmd.arg("-NoProfile");
        cmd.arg("-NonInteractive");
        cmd.arg("-NoLogo");
        cmd
    }
}
