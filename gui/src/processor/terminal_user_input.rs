use imgui::{Key, Ui};
use wezterm_term::{KeyCode, KeyModifiers};

#[derive(Debug, Clone, Default)]
pub struct UserInputState {
    // mouse: MouseState,
    pub keyboard: KeyState,
}

// #[derive(Debug, Eq, PartialEq, Clone, Default)]
// pub struct MouseState {
//     moving: bool,
//     left_pressed: bool,
//     right_pressed: bool,
//     middle_pressed: bool,
//     scroll: f32,
//     grid_x: usize,
//     grid_y: VisibleRowIndex,
//     x_pixel_offset: isize,
//     y_pixel_offset: isize,
// }
//
// impl MouseState {
//     pub fn is_pressed(&self) -> bool {
//         self.left_pressed || self.middle_pressed || self.right_pressed
//     }
//
//     fn different_pressed(&self, other: &MouseState) -> bool {
//         self.is_pressed() && other.is_pressed() && (
//             self.left_pressed != other.left_pressed ||
//             self.middle_pressed != other.middle_pressed ||
//             self.right_pressed != other.right_pressed
//         )
//     }
//
//     fn released_buttons(&self, other: &MouseState) -> Vec<MouseButton> {
//         let mut vec = vec![];
//         if other.left_pressed && !self.left_pressed {
//             vec.push(MouseButton::Left);
//         }
//
//         if other.middle_pressed && !self.middle_pressed {
//             vec.push(MouseButton::Middle);
//         }
//
//         if other.right_pressed && !self.right_pressed {
//             vec.push(MouseButton::Right);
//         }
//
//         vec
//     }
//
//     fn pressed_buttons(&self, other: &MouseState) -> Vec<MouseButton> {
//         other.released_buttons(self)
//     }
//
//     pub fn create_events(&self, last_event: &MouseState, key_modifiers: KeyModifiers) -> Vec<MouseEvent> {
//         let released_buttons = self.released_buttons(last_event);
//         let pressed_buttons = self.pressed_buttons(last_event);
//
//         let mut events = released_buttons.iter()
//             .map(|btn| new_mouse_event(self, *btn, MouseEventKind::Release, key_modifiers))
//             .collect::<Vec<_>>();
//
//         if !pressed_buttons.is_empty() {
//             events.extend(pressed_buttons.iter()
//                 .map(|btn| new_mouse_event(self, *btn, MouseEventKind::Press, key_modifiers)));
//         }
//         else if self.moving {
//             events.push(new_mouse_event(self, MouseButton::None, MouseEventKind::Move, key_modifiers));
//         }
//
//         events
//     }
// }
//
// fn new_mouse_event(state: &MouseState, button: MouseButton, kind: MouseEventKind, modifiers: KeyModifiers) -> MouseEvent {
//     MouseEvent {
//         kind,
//         x: state.grid_x,
//         y: state.grid_y,
//         x_pixel_offset: state.x_pixel_offset,
//         y_pixel_offset: state.y_pixel_offset,
//         button,
//         modifiers,
//     }
// }

#[derive(Debug, Eq, PartialEq, Clone, Default)]
pub struct KeyState {
    pub modifiers: KeyModifiers,
    pub key_codes: Vec<KeyCode>,
}

impl KeyState {
    pub fn is_empty(&self) -> bool {
        self.modifiers == KeyModifiers::NONE && self.key_codes.is_empty()
    }
}

pub fn determine_keys_pressed(ui: &Ui) -> KeyState {
    let mut modifiers: KeyModifiers = KeyModifiers::NONE;
    let mut key_codes: Vec<KeyCode> = vec![];
    
    Key::VARIANTS.iter().for_each(|k| {
        let key = *k;
        let pressed = ui.is_key_pressed(key);
        let down = ui.is_key_down(key);
        if !pressed && !down {
            return;
        }
        
        let Some(key_code) = key_to_key_code(key) else {
            return;
        };
        
        let mut is_modifier = false;
        if down {
            // `down` boolean is only used for modifiers
            // because when a modifier like CTRL is pressed, then another key is pressed,
            // the modifier no longer shows as pressed, but will show as down
            // this means events like CTRL-C will not send without doing it this way
            is_modifier = true;
            match key_code {
                KeyCode::Super => {
                    modifiers |= KeyModifiers::SUPER;
                }
                KeyCode::Shift |
                KeyCode::LeftShift |
                KeyCode::RightShift => {
                    modifiers |= KeyModifiers::SHIFT;
                }
                KeyCode::Control |
                KeyCode::LeftControl |
                KeyCode::RightControl => {
                    modifiers |= KeyModifiers::CTRL;
                }
                KeyCode::Alt |
                KeyCode::LeftAlt |
                KeyCode::RightAlt => {
                    modifiers |= KeyModifiers::ALT;
                }
                _ => {
                    is_modifier = false;
                }
            }
        }
        
        if pressed && !is_modifier {
            key_codes.push(key_code);
        }
    });
    
    KeyState {
        modifiers,
        key_codes,
    }
}

fn key_to_key_code(key: Key) -> Option<KeyCode> {
    match key {
        Key::Tab => Some(KeyCode::Tab),
        Key::LeftArrow => Some(KeyCode::LeftArrow),
        Key::RightArrow => Some(KeyCode::RightArrow),
        Key::UpArrow => Some(KeyCode::UpArrow),
        Key::DownArrow => Some(KeyCode::DownArrow),
        Key::PageUp => Some(KeyCode::PageUp),
        Key::PageDown => Some(KeyCode::PageDown),
        Key::Home => Some(KeyCode::Home),
        Key::End => Some(KeyCode::End),
        Key::Insert => Some(KeyCode::Insert),
        Key::Delete => Some(KeyCode::Char('\u{7f}')),
        Key::Backspace => Some(KeyCode::Char('\u{8}')),
        Key::Space => Some(KeyCode::Char(' ')),
        Key::Enter => Some(KeyCode::Char('\r')),
        Key::Escape => Some(KeyCode::Char('\x1b')),
        Key::LeftCtrl => Some(KeyCode::LeftControl),
        Key::LeftShift => Some(KeyCode::LeftShift),
        Key::LeftAlt => Some(KeyCode::LeftAlt),
        Key::LeftSuper => Some(KeyCode::Super),
        Key::RightCtrl => Some(KeyCode::RightControl),
        Key::RightShift => Some(KeyCode::RightShift),
        Key::RightAlt => Some(KeyCode::RightAlt),
        Key::RightSuper => Some(KeyCode::Super),
        Key::Menu => Some(KeyCode::Menu),
        Key::Alpha0 => Some(KeyCode::Char('0')),
        Key::Alpha1 => Some(KeyCode::Char('1')),
        Key::Alpha2 => Some(KeyCode::Char('2')),
        Key::Alpha3 => Some(KeyCode::Char('3')),
        Key::Alpha4 => Some(KeyCode::Char('4')),
        Key::Alpha5 => Some(KeyCode::Char('5')),
        Key::Alpha6 => Some(KeyCode::Char('6')),
        Key::Alpha7 => Some(KeyCode::Char('7')),
        Key::Alpha8 => Some(KeyCode::Char('8')),
        Key::Alpha9 => Some(KeyCode::Char('9')),
        Key::A => Some(KeyCode::Char('a')),
        Key::B => Some(KeyCode::Char('b')),
        Key::C => Some(KeyCode::Char('c')),
        Key::D => Some(KeyCode::Char('d')),
        Key::E => Some(KeyCode::Char('e')),
        Key::F => Some(KeyCode::Char('f')),
        Key::G => Some(KeyCode::Char('g')),
        Key::H => Some(KeyCode::Char('h')),
        Key::I => Some(KeyCode::Char('i')),
        Key::J => Some(KeyCode::Char('j')),
        Key::K => Some(KeyCode::Char('k')),
        Key::L => Some(KeyCode::Char('l')),
        Key::M => Some(KeyCode::Char('m')),
        Key::N => Some(KeyCode::Char('n')),
        Key::O => Some(KeyCode::Char('o')),
        Key::P => Some(KeyCode::Char('p')),
        Key::Q => Some(KeyCode::Char('q')),
        Key::R => Some(KeyCode::Char('r')),
        Key::S => Some(KeyCode::Char('s')),
        Key::T => Some(KeyCode::Char('t')),
        Key::U => Some(KeyCode::Char('u')),
        Key::V => Some(KeyCode::Char('v')),
        Key::W => Some(KeyCode::Char('w')),
        Key::X => Some(KeyCode::Char('x')),
        Key::Y => Some(KeyCode::Char('y')),
        Key::Z => Some(KeyCode::Char('z')),
        Key::F1 => Some(KeyCode::Function(1)),
        Key::F2 => Some(KeyCode::Function(2)),
        Key::F3 => Some(KeyCode::Function(3)),
        Key::F4 => Some(KeyCode::Function(4)),
        Key::F5 => Some(KeyCode::Function(5)),
        Key::F6 => Some(KeyCode::Function(6)),
        Key::F7 => Some(KeyCode::Function(7)),
        Key::F8 => Some(KeyCode::Function(8)),
        Key::F9 => Some(KeyCode::Function(9)),
        Key::F10 => Some(KeyCode::Function(10)),
        Key::F11 => Some(KeyCode::Function(11)),
        Key::F12 => Some(KeyCode::Function(12)),
        Key::Apostrophe => Some(KeyCode::Char('\'')),
        Key::Comma => Some(KeyCode::Char(',')),
        Key::Minus => Some(KeyCode::Char('-')),
        Key::Period => Some(KeyCode::Char('.')),
        Key::Slash => Some(KeyCode::Char('/')),
        Key::Semicolon => Some(KeyCode::Char(';')),
        Key::Equal => Some(KeyCode::Char('=')),
        Key::LeftBracket => Some(KeyCode::Char('[')),
        Key::Backslash => Some(KeyCode::Char('\\')),
        Key::RightBracket => Some(KeyCode::Char(']')),
        Key::GraveAccent => Some(KeyCode::Char('`')),
        Key::CapsLock => Some(KeyCode::CapsLock),
        Key::ScrollLock => Some(KeyCode::ScrollLock),
        Key::NumLock => Some(KeyCode::NumLock),
        Key::PrintScreen => Some(KeyCode::PrintScreen),
        Key::Pause => Some(KeyCode::Pause),
        Key::Keypad0 => Some(KeyCode::Numpad0),
        Key::Keypad1 => Some(KeyCode::Numpad1),
        Key::Keypad2 => Some(KeyCode::Numpad2),
        Key::Keypad3 => Some(KeyCode::Numpad3),
        Key::Keypad4 => Some(KeyCode::Numpad4),
        Key::Keypad5 => Some(KeyCode::Numpad5),
        Key::Keypad6 => Some(KeyCode::Numpad6),
        Key::Keypad7 => Some(KeyCode::Numpad7),
        Key::Keypad8 => Some(KeyCode::Numpad8),
        Key::Keypad9 => Some(KeyCode::Numpad9),
        Key::KeypadDecimal => Some(KeyCode::Decimal),
        Key::KeypadDivide => Some(KeyCode::Divide),
        Key::KeypadMultiply => Some(KeyCode::Multiply),
        Key::KeypadSubtract => Some(KeyCode::Subtract),
        Key::KeypadAdd => Some(KeyCode::Add),
        Key::KeypadEnter => Some(KeyCode::Char('\r')),
        Key::KeypadEqual => Some(KeyCode::Char('=')),
        Key::ReservedForModCtrl => Some(KeyCode::Control),
        Key::ReservedForModShift => Some(KeyCode::Shift),
        Key::ReservedForModAlt => Some(KeyCode::Alt),
        Key::ReservedForModSuper => Some(KeyCode::Super),
        Key::ModCtrl => Some(KeyCode::Control),
        Key::ModShift => Some(KeyCode::Shift),
        Key::ModAlt => Some(KeyCode::Alt),
        Key::ModSuper => Some(KeyCode::Super),
        _ => None
    }
}