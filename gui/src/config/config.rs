use wezterm_term::color::ColorPalette;
use wezterm_term::TerminalConfiguration;

#[derive(Debug, Default)]
pub struct DyvytConfig {
}

impl TerminalConfiguration for DyvytConfig {
    fn color_palette(&self) -> ColorPalette {
        ColorPalette::default()
    }
}