use std::cell::{RefCell};
use std::rc::Rc;
use imgui::{InputTextCallbackHandler, InputTextMultilineCallback, sys, Ui};
use imgui::sys::ImVec2;
use log::error;
use crate::gui::terminal_view::TerminalView;
use crate::resource::ResourceInfo;

#[derive(Debug)]
pub struct TerminalApp {
    outputs: Vec<TerminalView>,
    input_text: String,
    output_window_size: [f32; 2],
    resource_info: Box<ResourceInfo>,
}

impl TerminalApp {
    pub fn new(resource_info: Box<ResourceInfo>) -> Self {
        Self {
            outputs: vec![],
            input_text: "".to_owned(),
            output_window_size: [0., 0.],
            resource_info,
        }
    }
    
    pub fn draw(&mut self, ui: &mut Ui) {
        unsafe {
            let viewport = sys::igGetMainViewport();
            sys::igSetNextWindowPos((*viewport).WorkPos, sys::ImGuiCond_Always as sys::ImGuiCond, ImVec2 { x: 0.0, y: 0.0 });
            sys::igSetNextWindowSize((*viewport).WorkSize, sys::ImGuiCond_Always as sys::ImGuiCond);
        }
        ui.window("Main Window")
            .movable(false)
            .resizable(false)
            .collapsible(false)
            .title_bar(false)
            .build(|| {
                self.calculate_output_window_size(ui);
                
                ui.child_window("child window")
                    .size(self.output_window_size)
                    .horizontal_scrollbar(true)
                    .build(|| {
                        for out in &mut self.outputs {
                            out.draw(ui);
                        }
                        let dl = ui.get_window_draw_list();
                        dl.add_text([1000.0, 5.], [1., 1., 1., 1.], format!("{} fps", ui.io().framerate.round()));
                    });
                ui.child_window("input window")
                    .size(ui.content_region_avail())
                    .build(|| {
                        let shift_down = Rc::new(ui.io().key_shift);
                        let should_run_command = Rc::new(RefCell::new(false));
                        let callback = InCallback {
                            shift_down: shift_down.clone(),
                            should_run_command: should_run_command.clone(),
                        };
                        ui.input_text_multiline("Label", &mut self.input_text, ui.content_region_avail())
                            .callback(InputTextMultilineCallback::CHAR_FILTER, callback)
                            .build();
                        
                        if (*should_run_command).take() {
                            self.input_text = self.input_text.trim().to_owned();
                            if cfg!(target_os = "windows") {
                                self.input_text += "\r\n";
                            }
                            else {
                                self.input_text += "\n";
                            }
                            self.run_command();
                        }
                    });
            });
    }
    
    fn calculate_output_window_size(&mut self, ui: &Ui) {
        let avail = ui.content_region_avail();
        let win_width = avail[0];
        let win_height = avail[1];
        let line_height = ui.text_line_height();
        let num_lines = 1 + self.input_text.chars().filter(|c| *c == '\n').count();
        let input_win_height = 10. + (num_lines as f32 * line_height);
        self.output_window_size = [
            win_width,
            (win_height - input_win_height).clamp(
                (win_height/2.).max(0.),
                win_height.max(0.),
            ),
        ];
    }
    
    #[tracing::instrument]
    fn run_command(&mut self) {
        match TerminalView::new(self.input_text.clone(), self.resource_info.clone()) {
            Ok(output) => {
                self.outputs.push(output);
                self.input_text.clear();
            }
            Err(e) => {
                error!("{e}");
            }
        };
    }
}

struct InCallback {
    shift_down: Rc<bool>,
    should_run_command: Rc<RefCell<bool>>,
}
impl InputTextCallbackHandler for InCallback {
    fn char_filter(&mut self, c: char) -> Option<char> {
        if c == '\n' && !*self.shift_down {
            (*self.should_run_command).replace(true);
        }

        Some(c)
    }
}