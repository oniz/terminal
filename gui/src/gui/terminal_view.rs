use std::fmt::{Debug, Formatter};
use std::sync::atomic::{AtomicU64, Ordering};
use std::time::{Instant};
use imgui::{ListClipper, StyleColor, StyleVar, TreeNodeFlags, Ui};
use log::info;
use once_cell::sync::Lazy;
use wezterm_term::{Intensity, KeyCode, KeyModifiers, TerminalSize};
use wezterm_term::color::ColorAttribute;
use crate::gui::colors::{color_from_srgba_tuple, index_to_color};
use crate::processor::command_context::{CommandContext};
use crate::processor::terminal_user_input::{determine_keys_pressed, UserInputState};
use crate::resource::ResourceInfo;
use crate::utils::ui_utils::window_absolute_bounds;

static COUNTER: Lazy<AtomicU64> = Lazy::new(|| AtomicU64::new(0));

pub struct TerminalView {
    title: String,
    context: CommandContext,
    active: bool,
    focused: bool,
    create_time: Instant,
    last_send: Instant,
    resource_info: Box<ResourceInfo>,
    terminal_size: TerminalSize,
    user_input: UserInputState,
}

impl Debug for TerminalView {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("TerminalView")
            .field("title", &self.title)
            .finish()
    }
}

impl TerminalView {
    pub fn new(command: String, resource_info: Box<ResourceInfo>) -> anyhow::Result<Self> {
        let terminal_size = TerminalSize {
            rows: 30,
            cols: 100,
            pixel_height: 0,
            pixel_width: 0,
            dpi: 0,
        };
        let rand_id = COUNTER.fetch_add(1, Ordering::Relaxed);
        let title = format!("{}##{}", command.replace("\\\n", " ").replace('\n', " "), rand_id);
        let context = CommandContext::new(terminal_size, command)?;
        
        Ok(Self {
            title,
            context,
            active: true,
            focused: true,
            create_time: Instant::now(),
            last_send: Instant::now(),
            terminal_size,
            resource_info,
            user_input: UserInputState::default(),
        })
    }
    
    pub fn draw(&mut self, ui: &Ui) {
        let space_size = ui.calc_text_size(" ");
        let window_size = [
            self.terminal_size.cols as f32 * space_size[0] + space_size[0] * 3.,
            self.terminal_size.rows as f32 * space_size[1],
        ];
        
        if ui.collapsing_header(&self.title, TreeNodeFlags::DEFAULT_OPEN | TreeNodeFlags::FRAMED) {
            let _border = if self.focused {
                Some(ui.push_style_color(StyleColor::Border, [1., 1., 1., 1.]))
            }
            else {
                None
            };
            
            ui.child_window(&self.title)
                .movable(false)
                .size(window_size)
                .border(true)
                .build(|| {
                    let dl = ui.get_window_draw_list();
                    let mut guard = self.context.terminal.lock().unwrap();
                    let terminal = &mut *guard;
                    let screen = terminal.screen_mut();
                    let num_lines = screen.scrollback_rows();
                    let line_height = ui.text_line_height();
                    let clipper = ListClipper::new(num_lines as i32)
                        .items_height(line_height)
                        .begin(ui);
                    
                    // without this, each text char will be too spaced out from the next letter
                    let _spacing_style = ui.push_style_var(StyleVar::ItemSpacing([0., 0.]));
                    for row_num in clipper.iter() {
                        let line = screen.line_mut(row_num as usize);
                        for i in 0..line.len() {
                            let cell = &line.cells_mut()[i];
                            let str = cell.str();
                            
                            if str.is_empty() {
                                let cursor = ui.cursor_pos();
                                ui.set_cursor_pos([
                                    cursor[0] + space_size[0],
                                    cursor[1],
                                ]);
                                continue;
                            }
                            
                            let attrs = cell.attrs();
                            let _f = ui.push_font(match attrs.intensity() {
                                Intensity::Normal => self.resource_info.regular_font,
                                Intensity::Bold => self.resource_info.bold_font,
                                Intensity::Half => self.resource_info.light_font,
                            });
                            
                            let _fg = match attrs.foreground() {
                                ColorAttribute::TrueColorWithPaletteFallback(color, _) => {
                                    ui.push_style_color(StyleColor::Text, color_from_srgba_tuple(color))
                                }
                                ColorAttribute::TrueColorWithDefaultFallback(color) => {
                                    ui.push_style_color(StyleColor::Text, color_from_srgba_tuple(color))
                                }
                                ColorAttribute::PaletteIndex(i) => {
                                    ui.push_style_color(StyleColor::Text, index_to_color(i))
                                }
                                ColorAttribute::Default => ui.push_style_color(StyleColor::Text, [1., 1., 1., 1.])
                            };
                            let background_color = attrs.background();
                            let crossed_out = attrs.strikethrough();
                            let underline_method = attrs.underline() as u8;
                            // get corner bounds in case it's needed below
                            let corners = if !matches!(background_color, ColorAttribute::Default) || crossed_out || underline_method > 0 {
                                Some(self.get_bounds_for_draw(ui, &space_size))
                            }
                            else {
                                None
                            };
                            
                            // draw the background color before the text
                            match background_color {
                                ColorAttribute::TrueColorWithPaletteFallback(color, _) => {
                                    let (p1, p2) = corners.expect("(p1, p2) should be set for drawing background");
                                    dl.add_rect(p1, p2, color_from_srgba_tuple(color)).filled(true).build();
                                }
                                ColorAttribute::TrueColorWithDefaultFallback(color) => {
                                    let (p1, p2) = corners.expect("(p1, p2) should be set for drawing background");
                                    dl.add_rect(p1, p2, color_from_srgba_tuple(color)).filled(true).build();
                                }
                                ColorAttribute::PaletteIndex(i) => {
                                    let (p1, p2) = corners.expect("(p1, p2) should be set for drawing background");
                                    dl.add_rect(p1, p2, index_to_color(i)).filled(true).build();
                                }
                                ColorAttribute::Default => {}
                            };
                            
                            // alternate the text color if blinking is set
                            let _blink_token = if attrs.blink() as u8 > 0 && self.create_time.elapsed().as_secs() % 2 == 0 {
                                Some(ui.push_style_color(StyleColor::Text, ui.style_color(StyleColor::TextDisabled)))
                            }
                            else {
                                None
                            };
                            
                            ui.text(str);
                            ui.same_line();
                            
                            // draw crossed out on top of text
                            if crossed_out {
                                let (p1, p2) = corners.expect("(p1, p2) should be set for drawing crossed out line");
                                let current_color = ui.style_color(StyleColor::Text);
                                let mid_y = p1[1] + (p2[1] - p1[1]) / 2.;
                                dl.add_line([p1[0], mid_y], [p2[0], mid_y], current_color).build();
                            }
                            
                            // draw underline on top of text
                            if underline_method > 0 {
                                let (p1, mut p2) = corners.expect("(p1, p2) should be set for drawing underline");
                                let current_color = ui.style_color(StyleColor::Text);
                                p2[1] += (ui.current_font().descent / 2.).floor(); // move the line up just a bit
                                dl.add_line([p1[0], p2[1]], p2, current_color).build();
                                if underline_method >= 2 {
                                    p2[1] += 2.;
                                    dl.add_line([p1[0], p2[1]], p2, current_color).build();
                                }
                            }
                        }
                        ui.new_line();
                    }
                    
                    let (win_min, win_max) = window_absolute_bounds(ui);
                    let mouse_inside = ui.is_mouse_hovering_rect(win_min, win_max);
                    if ui.is_any_mouse_down() {
                        self.focused = mouse_inside;
                    }
                    
                    if self.focused {
                        let mut should_paste = false;
                        // handle keyboard
                        let new_key_state = determine_keys_pressed(ui);
                        if new_key_state != self.user_input.keyboard && !new_key_state.is_empty() {
                            for kc in &new_key_state.key_codes {
                                if new_key_state.modifiers.contains(KeyModifiers::CTRL) {
                                    if matches!(kc, KeyCode::Char('v')) {
                                        // paste
                                        // terminal.set_clipboard()
                                    }
                                }
                                terminal.key_down(*kc, new_key_state.modifiers).unwrap_or_default();
                                info!("sending {kc:?} {:?}", new_key_state.modifiers);
                            }
                        }
                        self.user_input.keyboard = new_key_state;
                    }
                });
        }
        else {
            self.focused = false;
        }
    }
    
    fn get_bounds_for_draw(&self, ui: &Ui, char_size: &[f32; 2]) -> ([f32; 2], [f32; 2]) {
        let mut p1 = ui.cursor_pos();
        let win_pos = ui.window_pos();
        p1[0] += win_pos[0] - ui.scroll_x();
        p1[1] += win_pos[1] - ui.scroll_y();
        let p2 = [p1[0] + char_size[0], p1[1] + char_size[1]];
        
        (p1, p2)
    }
}
