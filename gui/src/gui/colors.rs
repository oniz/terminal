use once_cell::sync::Lazy;
use wezterm_term::color::SrgbaTuple;

pub fn ansi_color_to_rgb(params: (u8, u8, u8, u8, u8)) -> Option<FloatColor> {
    match params {
        (30, _, _, _, _) |
        (40, _, _, _, _) => Some(_256_COLORS[0]), // black
        (31, _, _, _, _) |
        (41, _, _, _, _) => Some(_256_COLORS[1]), // red
        (32, _, _, _, _) |
        (42, _, _, _, _)  => Some(_256_COLORS[2]), // green
        (33, _, _, _, _) |
        (43, _, _, _, _) => Some(_256_COLORS[3]), // yellow
        (34, _, _, _, _) |
        (44, _, _, _, _) => Some(_256_COLORS[4]), // blue
        (35, _, _, _, _) |
        (45, _, _, _, _) => Some(_256_COLORS[5]), // magenta
        (36, _, _, _, _) |
        (46, _, _, _, _) => Some(_256_COLORS[6]), // cyan
        (37, _, _, _, _) |
        (47, _, _, _, _) => Some(_256_COLORS[7]), // light gray (dark white)
        (90, _, _, _, _) |
        (100, _, _, _, _) => Some(_256_COLORS[8]), // bright black (gray)
        (91, _, _, _, _) |
        (101, _, _, _, _) => Some(_256_COLORS[9]), // bright red
        (92, _, _, _, _) |
        (102, _, _, _, _) => Some(_256_COLORS[10]), // bright green
        (93, _, _, _, _) |
        (103, _, _, _, _) => Some(_256_COLORS[11]), // bright yellow
        (94, _, _, _, _) |
        (104, _, _, _, _) => Some(_256_COLORS[12]), // bright blue
        (95, _, _, _, _) |
        (105, _, _, _, _) => Some(_256_COLORS[13]), // bright magenta
        (96, _, _, _, _) |
        (106, _, _, _, _) => Some(_256_COLORS[14]), // bright cyan
        (97, _, _, _, _) |
        (107, _, _, _, _) => Some(_256_COLORS[15]), // white
        (38, 5, i, _, _) |
        (48, 5, i, _, _) => Some(_256_COLORS[i as usize]),
        (38, 2, r, g, b) |
        (48, 2, r, g, b) => Some(rgb_convert(r, g, b)),
        _ => None
    }
}

pub fn index_to_color(i: u8) -> FloatColor {
    _256_COLORS[i as usize]
}

pub type FloatColor = [f32; 4];

pub fn color_from_srgba_tuple(t: SrgbaTuple) -> FloatColor {
    [t.0, t.1, t.2, t.3]
}

fn rgb_convert(r: u8, g: u8, b: u8) -> FloatColor {
    [r as f32 / 255., g as f32 / 255., b as f32 / 255., 1.]
}

static _256_COLORS: Lazy<[FloatColor; 256]> = Lazy::new(|| [
    rgb_convert(0, 0, 0),
    rgb_convert(142, 0, 0),
    rgb_convert(0, 142, 0),
    rgb_convert(142, 142, 0),
    rgb_convert(0, 55, 218),
    rgb_convert(142, 0, 142),
    rgb_convert(0, 142, 142),
    rgb_convert(142, 142, 142),
    rgb_convert(51, 51, 51),
    rgb_convert(214, 51, 51),
    rgb_convert(51, 214, 51),
    rgb_convert(214, 214, 51),
    rgb_convert(59, 120, 255),
    rgb_convert(214, 51, 214),
    rgb_convert(51, 214, 214),
    rgb_convert(214, 214, 214),
    rgb_convert(0, 0, 0),
    rgb_convert(0, 0, 51),
    rgb_convert(0, 0, 102),
    rgb_convert(0, 0, 153),
    rgb_convert(0, 0, 204),
    rgb_convert(0, 0, 255),
    rgb_convert(0, 51, 0),
    rgb_convert(0, 51, 51),
    rgb_convert(0, 51, 102),
    rgb_convert(0, 51, 153),
    rgb_convert(0, 51, 204),
    rgb_convert(0, 51, 255),
    rgb_convert(0, 102, 0),
    rgb_convert(0, 102, 51),
    rgb_convert(0, 102, 102),
    rgb_convert(0, 102, 153),
    rgb_convert(0, 102, 204),
    rgb_convert(0, 102, 255),
    rgb_convert(0, 153, 0),
    rgb_convert(0, 153, 51),
    rgb_convert(0, 153, 102),
    rgb_convert(0, 153, 153),
    rgb_convert(0, 153, 204),
    rgb_convert(0, 153, 255),
    rgb_convert(0, 204, 0),
    rgb_convert(0, 204, 51),
    rgb_convert(0, 204, 102),
    rgb_convert(0, 204, 153),
    rgb_convert(0, 204, 204),
    rgb_convert(0, 204, 255),
    rgb_convert(0, 255, 0),
    rgb_convert(0, 255, 51),
    rgb_convert(0, 255, 102),
    rgb_convert(0, 255, 153),
    rgb_convert(0, 255, 204),
    rgb_convert(0, 255, 255),
    rgb_convert(51, 0, 0),
    rgb_convert(51, 0, 51),
    rgb_convert(51, 0, 102),
    rgb_convert(51, 0, 153),
    rgb_convert(51, 0, 204),
    rgb_convert(51, 0, 255),
    rgb_convert(51, 51, 0),
    rgb_convert(51, 51, 51),
    rgb_convert(51, 51, 102),
    rgb_convert(51, 51, 153),
    rgb_convert(51, 51, 204),
    rgb_convert(51, 51, 255),
    rgb_convert(51, 102, 0),
    rgb_convert(51, 102, 51),
    rgb_convert(51, 102, 102),
    rgb_convert(51, 102, 153),
    rgb_convert(51, 102, 204),
    rgb_convert(51, 102, 255),
    rgb_convert(51, 153, 0),
    rgb_convert(51, 153, 51),
    rgb_convert(51, 153, 102),
    rgb_convert(51, 153, 153),
    rgb_convert(51, 153, 204),
    rgb_convert(51, 153, 255),
    rgb_convert(51, 204, 0),
    rgb_convert(51, 204, 51),
    rgb_convert(51, 204, 102),
    rgb_convert(51, 204, 153),
    rgb_convert(51, 204, 204),
    rgb_convert(51, 204, 255),
    rgb_convert(51, 255, 0),
    rgb_convert(51, 255, 51),
    rgb_convert(51, 255, 102),
    rgb_convert(51, 255, 153),
    rgb_convert(51, 255, 204),
    rgb_convert(51, 255, 255),
    rgb_convert(102, 0, 0),
    rgb_convert(102, 0, 51),
    rgb_convert(102, 0, 102),
    rgb_convert(102, 0, 153),
    rgb_convert(102, 0, 204),
    rgb_convert(102, 0, 255),
    rgb_convert(102, 51, 0),
    rgb_convert(102, 51, 51),
    rgb_convert(102, 51, 102),
    rgb_convert(102, 51, 153),
    rgb_convert(102, 51, 204),
    rgb_convert(102, 51, 255),
    rgb_convert(102, 102, 0),
    rgb_convert(102, 102, 51),
    rgb_convert(102, 102, 102),
    rgb_convert(102, 102, 153),
    rgb_convert(102, 102, 204),
    rgb_convert(102, 102, 255),
    rgb_convert(102, 153, 0),
    rgb_convert(102, 153, 51),
    rgb_convert(102, 153, 102),
    rgb_convert(102, 153, 153),
    rgb_convert(102, 153, 204),
    rgb_convert(102, 153, 255),
    rgb_convert(102, 204, 0),
    rgb_convert(102, 204, 51),
    rgb_convert(102, 204, 102),
    rgb_convert(102, 204, 153),
    rgb_convert(102, 204, 204),
    rgb_convert(102, 204, 255),
    rgb_convert(102, 255, 0),
    rgb_convert(102, 255, 51),
    rgb_convert(102, 255, 102),
    rgb_convert(102, 255, 153),
    rgb_convert(102, 255, 204),
    rgb_convert(102, 255, 255),
    rgb_convert(153, 0, 0),
    rgb_convert(153, 0, 51),
    rgb_convert(153, 0, 102),
    rgb_convert(153, 0, 153),
    rgb_convert(153, 0, 204),
    rgb_convert(153, 0, 255),
    rgb_convert(153, 51, 0),
    rgb_convert(153, 51, 51),
    rgb_convert(153, 51, 102),
    rgb_convert(153, 51, 153),
    rgb_convert(153, 51, 204),
    rgb_convert(153, 51, 255),
    rgb_convert(153, 102, 0),
    rgb_convert(153, 102, 51),
    rgb_convert(153, 102, 102),
    rgb_convert(153, 102, 153),
    rgb_convert(153, 102, 204),
    rgb_convert(153, 102, 255),
    rgb_convert(153, 153, 0),
    rgb_convert(153, 153, 51),
    rgb_convert(153, 153, 102),
    rgb_convert(153, 153, 153),
    rgb_convert(153, 153, 204),
    rgb_convert(153, 153, 255),
    rgb_convert(153, 204, 0),
    rgb_convert(153, 204, 51),
    rgb_convert(153, 204, 102),
    rgb_convert(153, 204, 153),
    rgb_convert(153, 204, 204),
    rgb_convert(153, 204, 255),
    rgb_convert(153, 255, 0),
    rgb_convert(153, 255, 51),
    rgb_convert(153, 255, 102),
    rgb_convert(153, 255, 153),
    rgb_convert(153, 255, 204),
    rgb_convert(153, 255, 255),
    rgb_convert(204, 0, 0),
    rgb_convert(204, 0, 51),
    rgb_convert(204, 0, 102),
    rgb_convert(204, 0, 153),
    rgb_convert(204, 0, 204),
    rgb_convert(204, 0, 255),
    rgb_convert(204, 51, 0),
    rgb_convert(204, 51, 51),
    rgb_convert(204, 51, 102),
    rgb_convert(204, 51, 153),
    rgb_convert(204, 51, 204),
    rgb_convert(204, 51, 255),
    rgb_convert(204, 102, 0),
    rgb_convert(204, 102, 51),
    rgb_convert(204, 102, 102),
    rgb_convert(204, 102, 153),
    rgb_convert(204, 102, 204),
    rgb_convert(204, 102, 255),
    rgb_convert(204, 153, 0),
    rgb_convert(204, 153, 51),
    rgb_convert(204, 153, 102),
    rgb_convert(204, 153, 153),
    rgb_convert(204, 153, 204),
    rgb_convert(204, 153, 255),
    rgb_convert(204, 204, 0),
    rgb_convert(204, 204, 51),
    rgb_convert(204, 204, 102),
    rgb_convert(204, 204, 153),
    rgb_convert(204, 204, 204),
    rgb_convert(204, 204, 255),
    rgb_convert(204, 255, 0),
    rgb_convert(204, 255, 51),
    rgb_convert(204, 255, 102),
    rgb_convert(204, 255, 153),
    rgb_convert(204, 255, 204),
    rgb_convert(204, 255, 255),
    rgb_convert(255, 0, 0),
    rgb_convert(255, 0, 51),
    rgb_convert(255, 0, 102),
    rgb_convert(255, 0, 153),
    rgb_convert(255, 0, 204),
    rgb_convert(255, 0, 255),
    rgb_convert(255, 51, 0),
    rgb_convert(255, 51, 51),
    rgb_convert(255, 51, 102),
    rgb_convert(255, 51, 153),
    rgb_convert(255, 51, 204),
    rgb_convert(255, 51, 255),
    rgb_convert(255, 102, 0),
    rgb_convert(255, 102, 51),
    rgb_convert(255, 102, 102),
    rgb_convert(255, 102, 153),
    rgb_convert(255, 102, 204),
    rgb_convert(255, 102, 255),
    rgb_convert(255, 153, 0),
    rgb_convert(255, 153, 51),
    rgb_convert(255, 153, 102),
    rgb_convert(255, 153, 153),
    rgb_convert(255, 153, 204),
    rgb_convert(255, 153, 255),
    rgb_convert(255, 204, 0),
    rgb_convert(255, 204, 51),
    rgb_convert(255, 204, 102),
    rgb_convert(255, 204, 153),
    rgb_convert(255, 204, 204),
    rgb_convert(255, 204, 255),
    rgb_convert(255, 255, 0),
    rgb_convert(255, 255, 51),
    rgb_convert(255, 255, 102),
    rgb_convert(255, 255, 153),
    rgb_convert(255, 255, 204),
    rgb_convert(255, 255, 255),
    rgb_convert(0, 0, 0),
    rgb_convert(10, 10, 10),
    rgb_convert(21, 21, 21),
    rgb_convert(32, 32, 32),
    rgb_convert(42, 42, 42),
    rgb_convert(53, 53, 53),
    rgb_convert(64, 64, 64),
    rgb_convert(74, 74, 74),
    rgb_convert(85, 85, 85),
    rgb_convert(96, 96, 96),
    rgb_convert(106, 106, 106),
    rgb_convert(117, 117, 117),
    rgb_convert(128, 128, 128),
    rgb_convert(138, 138, 138),
    rgb_convert(149, 149, 149),
    rgb_convert(160, 160, 160),
    rgb_convert(170, 170, 170),
    rgb_convert(181, 181, 181),
    rgb_convert(192, 192, 192),
    rgb_convert(202, 202, 202),
    rgb_convert(213, 213, 213),
    rgb_convert(224, 224, 224),
    rgb_convert(234, 234, 234),
    rgb_convert(245, 245, 245),
]);