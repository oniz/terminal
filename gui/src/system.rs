use glium::glutin;
use glium::glutin::event::{Event, WindowEvent};
use glium::glutin::event_loop::{ControlFlow, EventLoop};
use glium::glutin::window::WindowBuilder;
use glium::{Display, Surface};
use imgui::{Context, FontConfig, FontSource, Ui};
use imgui_glium_renderer::Renderer;
use imgui_winit_support::{HiDpiMode, WinitPlatform};
use std::path::Path;
use std::time::Instant;
use imgui_winit_support::winit::platform::run_return::EventLoopExtRunReturn;
use crate::clipboard;
use crate::resource::ResourceInfo;

pub struct System {
    pub event_loop: EventLoop<()>,
    pub display: Display,
    pub imgui: Context,
    pub platform: WinitPlatform,
    pub renderer: Renderer,
    pub font_size: f32,
    pub resource_info: Box<ResourceInfo>,
}

pub fn init(title: &str) -> System {
    let title = match Path::new(&title).file_name() {
        Some(file_name) => file_name.to_str().unwrap(),
        None => title,
    };
    let event_loop = EventLoop::new();
    let context = glutin::ContextBuilder::new().with_vsync(true);
    let builder = WindowBuilder::new()
        .with_title(title.to_owned())
        .with_inner_size(glutin::dpi::LogicalSize::new(1100., 768.))
        .with_position(glutin::dpi::LogicalPosition::new(2000., 1000.));
    let display = Display::new(builder, context, &event_loop).expect("Failed to initialize display");
    
    let mut imgui = Context::create();
    imgui.set_ini_filename(None);
    
    if let Some(backend) = clipboard::init() {
        imgui.set_clipboard_backend(backend);
    } else {
        eprintln!("Failed to initialize clipboard");
    }
    
    let mut platform = WinitPlatform::init(&mut imgui);
    {
        let gl_window = display.gl_window();
        let window = gl_window.window();
        
        let dpi_mode = if let Ok(factor) = std::env::var("IMGUI_EXAMPLE_FORCE_DPI_FACTOR") {
            // Allow forcing of HiDPI factor for debugging purposes
            match factor.parse::<f64>() {
                Ok(f) => HiDpiMode::Locked(f),
                Err(e) => panic!("Invalid scaling factor: {}", e),
            }
        } else {
            HiDpiMode::Default
        };
        
        platform.attach_window(imgui.io_mut(), window, dpi_mode);
    }
    
    // Fixed font size. Note imgui_winit_support uses "logical
    // pixels", which are physical pixels scaled by the devices
    // scaling factor. Meaning, 13.0 pixels should look the same size
    // on two different screens, and thus we do not need to scale this
    // value (as the scaling is handled by winit)
    let font_size = 20.0;
    let resource_info = load_fonts(font_size, &mut imgui);
    let renderer = Renderer::init(&mut imgui, &display).expect("Failed to initialize renderer");
    
    System {
        event_loop,
        display,
        imgui,
        platform,
        renderer,
        font_size,
        resource_info,
    }
}

impl System {
    pub fn main_loop<F: FnMut(&mut bool, &mut Ui) + 'static, E: Fn() + 'static>(self, mut run_ui: F, before_exit: E) {
        let System {
            mut event_loop,
            display,
            mut imgui,
            mut platform,
            mut renderer,
            ..
        } = self;
        let mut last_frame = Instant::now();
        // use run_return() instead of run() so that it doesn't just abort the whole program before this function returns
        event_loop.run_return(move |event, _, control_flow| {
            match event {
                Event::NewEvents(_) => {
                    let now = Instant::now();
                    imgui.io_mut().update_delta_time(now - last_frame);
                    last_frame = now;
                }
                Event::MainEventsCleared => {
                    let gl_window = display.gl_window();
                    platform
                        .prepare_frame(imgui.io_mut(), gl_window.window())
                        .expect("Failed to prepare frame");
                    gl_window.window().request_redraw();
                }
                Event::RedrawRequested(_) => {
                    let ui = imgui.frame();
                    
                    let mut run = true;
                    run_ui(&mut run, ui);
                    if !run {
                        *control_flow = ControlFlow::Exit;
                    }
                    
                    let gl_window = display.gl_window();
                    let mut target = display.draw();
                    target.clear_color_srgb(1.0, 1.0, 1.0, 1.0);
                    platform.prepare_render(ui, gl_window.window());
                    let draw_data = imgui.render();
                    renderer
                        .render(&mut target, draw_data)
                        .expect("Rendering failed");
                    target.finish().expect("Failed to swap buffers");
                }
                Event::WindowEvent {
                    event: WindowEvent::CloseRequested,
                    ..
                } => {
                    before_exit();
                    *control_flow = ControlFlow::Exit;
                },
                event => {
                    let gl_window = display.gl_window();
                    platform.handle_event(imgui.io_mut(), gl_window.window(), &event);
                }
            };
        });
    }
}

fn load_fonts(font_size: f32, imgui: &mut Context) -> Box<ResourceInfo> {
    let jb_mono_regular = include_bytes!("../resources/JetBrainsMono-Regular.ttf");
    let jb_mono_italic = include_bytes!("../resources/JetBrainsMono-Italic.ttf");
    let jb_mono_bold = include_bytes!("../resources/JetBrainsMono-ExtraBold.ttf");
    let jb_mono_light = include_bytes!("../resources/JetBrainsMono-ExtraLight.ttf");
    // let emoji = include_bytes!("../resources/NotoColorEmoji-Regular.ttf");
    
    let config = Some(FontConfig {
        // Oversampling font helps improve text rendering at
        // expense of larger font atlas texture.
        oversample_h: 4,
        oversample_v: 4,
        ..FontConfig::default()
    });
    // let emoji = FontSource::TtfData {
    //     data: emoji,
    //     size_pixels: font_size,
    //     config: Some(FontConfig {
    //         // Oversampling font helps improve text rendering at
    //         // expense of larger font atlas texture.
    //         oversample_h: 4,
    //         oversample_v: 4,
    //         ..FontConfig::default()
    //     }),
    // };
    
    let regular_font = imgui.fonts().add_font(&[
        FontSource::TtfData {
            data: jb_mono_regular,
            size_pixels: font_size,
            config: config.clone(),
        },
        // emoji,
    ]);
    let italic_font = imgui.fonts().add_font(&[
        FontSource::TtfData {
            data: jb_mono_italic,
            size_pixels: font_size,
            config: config.clone(),
        },
    ]);
    let bold_font = imgui.fonts().add_font(&[
        FontSource::TtfData {
            data: jb_mono_bold,
            size_pixels: font_size,
            config: config.clone(),
        },
    ]);
    let light_font = imgui.fonts().add_font(&[
        FontSource::TtfData {
            data: jb_mono_light,
            size_pixels: font_size,
            config: config.clone(),
        },
    ]);
    
    Box::new(ResourceInfo {
        regular_font,
        italic_font,
        bold_font,
        light_font,
    })
}