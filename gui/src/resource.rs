use imgui::FontId;

#[derive(Clone, Debug)]
pub struct ResourceInfo {
    pub regular_font: FontId,
    pub italic_font: FontId,
    pub bold_font: FontId,
    pub light_font: FontId,
}
