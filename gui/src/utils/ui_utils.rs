use imgui::Ui;

pub fn window_absolute_bounds(ui: &Ui) -> ([f32; 2], [f32; 2]) {
    let win_pos = ui.window_pos();
    let size = ui.window_size();
    (win_pos, [win_pos[0] + size[0], win_pos[1] + size[1]])
}
