use fast_log::Config;
use fast_log::consts::LogSize;
use fast_log::plugin::file_split::RollingType;
use fast_log::plugin::packer::LogPacker;
use log::{info, LevelFilter};
use tokio::runtime::Runtime;
use crate::gui::app::TerminalApp;

mod gui;
mod system;
mod clipboard;
mod processor;
mod resource;
mod config;
mod utils;
mod key_bindings;

#[tokio::main]
async fn main() {
    let rt = Runtime::new().unwrap();
    let _g = rt.enter();
    init_logger();
    run();
    info!("Ended Dyvyt");
    
    // necessary to kill all background async loops
    rt.shutdown_background();
}

fn run() {
    // let (flame_layer, _guard) = FlameLayer::with_file("./tracing.folded").unwrap();
    // let _g = flame_layer.flush_on_drop();
    // let subscriber = FmtSubscriber::builder()
    //     .with_max_level(tracing::Level::TRACE)
    //     .finish()
    //     .with(flame_layer);
    //
    // tracing::subscriber::set_global_default(subscriber)
    //     .expect("Unable to set global subscriber");
    
    let system = system::init("Dyvyt");
    let mut app = TerminalApp::new(system.resource_info.clone());
    info!("Started Dyvyt");
    system.main_loop(move |_, ui| {
        app.draw(ui);
    }, move || {
    });
}

fn init_logger() {
    fast_log::init(
        Config::new()
            .level(LevelFilter::Info)
            .file_split(
                "target/logs/",
                LogSize::MB(2),
                RollingType::KeepNum(10),
                LogPacker {}
            )
    ).unwrap();
}